package JFrame;

import java.awt.*;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class JFrameDemo {
    public void CreateJFrame() {
        JFrame jf = new JFrame("这是一个JFrame窗体");        // 实例化一个JFrame对象
        jf.setVisible(true);        // 设置窗体可视
        jf.setSize(500 , 350);        // 设置窗体大小
        jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);        // 设置窗体关闭方式
        JLabel jl = new JLabel("http://www.cnblogs.com/adamjwh/");        // 创建一个JLabel标签
        jl.setHorizontalAlignment(SwingConstants.CENTER);        // 使标签文字居中

        Container container = jf.getContentPane();        // 获取一个容器
        container.add(jl);        // 将标签添加至容器
        container.setBackground(Color.YELLOW);        // 设置容器背景颜色

        JButton jb = new JButton("点击弹出对话框");        // 创建按钮
        jb.setBounds(30 , 30 , 200 , 50);        // 按钮位置及大小
        jb.addActionListener(new ActionListener() {        // 监听器，用于监听点击事件
            @Override
            public void actionPerformed(ActionEvent e) {
                new JFrameDemo().CreateJFrame();
            }
        });
        container.add(jb);
    }

    public static void main(String[] args) {
        new JFrameDemo().CreateJFrame();        // 调用CreateJFrame()方法
    }
}
