package quickstart;

public interface ICalculate {
    final float PI = 3.1415f;        // 定义常量PI，表示圆周率

    //计算圆的面积
    float getArea(float r);            // 定义用于计算面积的方法getArea()

    //计算长方形的面积
    float getArea(float a , float b); //计算长方形面积

    float getCircumference(float r);    // 定义用于计算周长的方法getCircumference()


}
