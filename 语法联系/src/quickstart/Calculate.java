package quickstart;


//实现 ICalculate
public class Calculate implements ICalculate {
    @Override
    public float getArea(float r) {
        return PI * r * r;
    }

    @Override
    public float getCircumference(float r) {
        return 2 * PI * r;
    }

    @Override
    public float getArea(float a , float b) {
        return a * b;
    }
}
