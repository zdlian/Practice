package com.Package.Thread;

/*
自定义线程方法
* */
public class MyThread extends Thread {

    private static int i = 0;

    @Override
    public void run() {
        i++;
        System.out.println(i);
    }
}
