package quickstart;

public class Student {

    private int id;
    private String name;

    public Student() {
    }

    public Student(int id , String name) {
        this.id = id;
        this.name = name;
    }

    //设置id
    public void setId(int id) {
        this.id = id;
    }

    //设置名称
    public void setName(String name) {
        this.name = name;
    }

    //获取id
    public int getId() {
        return id;
    }

    //返回名称
    public String getName() {
        return name;
    }

    public Student getStudent() {
        return this;
    }

    @Override
    public String toString() {
        return String.format("姓名：%s 的同学; Id=%s" , this.name , this.id);
    }
}
