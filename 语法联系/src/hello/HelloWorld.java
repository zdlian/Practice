//标识符和关键字区分大小写
package hello;

//相关类的引用

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;
import java.io.*; //引入io 下的所以的包;
import java.lang.*;

//创建一个类
public class HelloWorld {

    //类的静态成员
    static int a = 0;

    //final 定义常量
    final static double PI = 3.1415926;

    //定义主函数
    public  static  void  main(String[] args) {
        //测试抽象类
        Cat cat = new Cat();
        cat.voice();
        Dog dog = new Dog();
        dog.voice();
        //测试
        System.out.println("this is a world");
        System.out.println(a);
        System.out.println(PI);

        //定义局部变量
        String str = "";
        str = "this is a jvbu var";
        System.out.println(str);
        //f2
        f2();
        //数据类型
        testdatatype();

        //界面输入
//        Scanner scanner=new Scanner(System.in);
//        System.out.println("今天是星期几：");
//        int week=scanner.nextInt();
//        switch (week){
//            case 1:
//                System.out.println("Today is Monday");
//                break;
//            case 2:
//                System.out.println("Today is Tuesday");
//                break;
//            case  3:
//                System.out.println("Today is Wednesday");
//                break;
//             default:
//                 System.out.println("Today is Other");
//                 break;
//        }

    }

    //j
    private static void f2() {
        System.out.println("this");
    }

    //数据类型
    private static void testdatatype() {
        //在Java中有8种数据类型，其中6种是数值类型，另外两种分别是字符类型和布尔类型
        byte myByte = 45; //-128 ~127
        short myShort = 777;//short型变量，也即短整型，占两个字节，取值范围-32768~32767
        int myInt = 12; //整形 int型变量，也即整型，占四个字节，取值范围-2147483648~2147483647
        long myLong = 22222;//长整型  占八个字节，取值范围-9223372036854775808~9223372036854775807

        //浮点型
        float myFloat = 0.22F; //单精度型  单精度浮点型，占四个字节，结尾必须加'F'或'f'，如果不加自动定义为double型变量，取值范围1.4E-45~3.4028235E-38

        double myDouble = 9893.0843;// 双精度型  双精度浮点型，占八个字节，结尾可以加'D'或'd'，也可不加，取值范围4.9E-324~1.7976931348623157E-308

        //字符
        char myChar = 'a';// 字符类型变量，用于存储单个字符，占两个字节，需用单引号括起来

        /* 布尔类型 */
        boolean isTrue = true; //// 布尔类型又称逻辑类型，只有true和false两个值，分别代表“真”和“假”

        //变量的定义
        int num = 0;
        System.out.println("Num 初始值：" + num);
        //赋值
        num = 100;
        System.out.println("Num 赋值后的值：" + num);

        //测试枚举
        FruseJuice fruseJuice = new FruseJuice();
        fruseJuice.size = FruseJuice.EnumFuseSize.Big;
        System.out.println(fruseJuice.size.toString());

        //测试类的消息属性
        Puppy puppy = new Puppy("miaomiao");
        //类的静态变量
        Puppy.color = "xxxxx";
        puppy.setAge(12);
        puppy.getAge();

        //String
        stringStr();
    }

    private static void stringStr() {
        String str = "  Hello World   ";
        System.out.println("输出O的位置" + str.indexOf('o'));
        System.out.println("输出O的位置" + str.lastIndexOf('o'));

        System.out.println("去除空格" + str.trim());
        System.out.println("去除All空格" + str.replaceAll(" " , ""));

        String str2 = "bad bad study";
        System.out.println("替换字符：" + str2.replace("bad" , "good"));

        String s1 = new String("http://www.cnblogs.com/adamjwh/");
        String s2 = new String("http://www.cnblogs.com/adamjwh/");
        String s3 = new String("HTTP://WWW.CNBLOGS.COM/ADAMJWH/");
        String s4 = s1;

        System.out.println("s1 == s2 : " + (s1 == s2));
        System.out.println("s1 == s4 : " + (s1 == s4));
        System.out.println("s1.equals(s2) : " + s1.equals(s2));
        System.out.println("s1.equals(s3) : " + s1.equals(s3));
        System.out.println("s1.equalsIgnoreCase(s2) : " + s1.equalsIgnoreCase(s2));
        System.out.println("s1.equalsIgnoreCase(s3) : " + s1.equalsIgnoreCase(s3));

        //大小写
        System.out.println("Str转大写" + str.toUpperCase());

        //StringBuilder
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ieuiewuiuwr");
        stringBuilder.append("sdfdskfjsk");
        stringBuilder.insert(1 , 38729429);
        System.out.println("stringBuilder" + stringBuilder);


    }
}

//枚举类
class FruseJuice {
    public enum EnumFuseSize {
        Big,
        Small,
        Liter
    }

    //实例变量
    public EnumFuseSize size;
}

//类的相关
class Puppy {
    int puppyAge;
    static String color;

    public Puppy(String name) {
        System.out.println("小狗的名字：" + name);
    }

    public void setAge(int age) {
        this.puppyAge = age;
    }

    public int getAge() {
        System.out.println("小狗的年龄：" + this.puppyAge);
        System.out.println("小狗的Color：" + color);
        //局部变量
        int a = 4;
        System.out.println(String.format("输出5 ++a =%s" , ++a));

        int b = 4;
        System.out.println(String.format("输出4 ++b =%s" , b++));

        System.out.println(String.format("输出后的值 ++b =%s" , b));

        //
        int i = 21;
        int j = 47;
        int z = i < j ? 100 : 200;
        System.out.println("输出：" + z);

        //强制类型转换
        int x = (int) 45.25F;
        long y = (long) 467.5f;
        System.out.println(String.format("强制类型转换：x=%s;y=%s" , x , y));

        //数组
        int[] week = {12 , 21 , 34 , 14 , 25 , 56 , 27};
//        for(int ii=1;ii<=7;ii++)
//        {
//            System.out.println(String.format("arr[%s]=%s",ii,week[ii]));
//        }
        for (int arr : week) {
            System.out.println(String.format("没有排序的值：%s" , arr));
        }
        //字典的排序
        Arrays.sort(week);
        for (int arr2 : week) {
            System.out.println(String.format("排序后的值：%s" , arr2));
        }

        return this.puppyAge;
    }

}