package com.Package.Thread;

/*线程同步机制*/
public class ThreadSafe implements Runnable {

    int num = 10;

    @Override
    public void run() {

        while (true) {
            synchronized ("") {        //加锁
                if (num > 0) {
                    try {
                        Thread.sleep(100);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    System.out.println("车票剩余：" + num-- + "张");
                }
            }
        }
    }
}
