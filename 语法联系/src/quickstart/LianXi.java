package quickstart;

import javax.print.attribute.standard.Finishings;

//测试高级语法
public class LianXi {

    public static void main(String[] args) {
        System.out.println("Hello World");
        //类的调用
        Example example = new Example("zhangdonglian");
        Student student = new Student();
        student.setId(34);
        student.setName("zhangdonglian");
        System.out.println(student.toString());

        //计算圆的面积
        float r = 4.3F;
        Calculate calculate = new Calculate();
        float area = calculate.getArea(r);
        float circumference = calculate.getCircumference(r);
        System.out.println(String.format("半径为r=%s;圆的周长=%s;圆的面积=%s" , r , area , circumference));
        //测试继承
        Dog dog = new Dog();
        dog.voice();
        Cat cat = new Cat();
        cat.voice();
        Fish fish = new Fish();
        fish.voice();

        //

    }


}



