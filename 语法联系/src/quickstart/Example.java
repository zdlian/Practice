package quickstart;

public class Example {
    public Example() {
        System.out.println("类的无参数的构造函数被实例");
    }

    public Example(String name) {
        System.out.println("类的有参数的构造函数被实例化," + name);
    }
}
