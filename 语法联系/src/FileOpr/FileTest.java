package FileOpr;

import java.io.*;
import java.lang.*;
import java.util.zip.ZipInputStream;

public class FileTest {

    public static void main(String[] args) {
        try {
            //File
            int b = 0;
            File file = new File("E:\\test.txt");
            FileInputStream readfile = new FileInputStream(file);
            byte buffer[] = new byte[2500]; //创建字节
            b = readfile.read(buffer , 1 , 2000);
            String str = new String(buffer , 0 , b , "Default");
            System.out.println(str);
            readfile.close(); //关闭文件流

            //创建输出流
            FileOutputStream writeFile = new FileOutputStream(file , true);
            writeFile.write(buffer , 0 , b);
            writeFile.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        /*文件大小*/
        getFile();

        /*缓存流文件*/
        bufferFile();


    }

    private static void getFile() {
        File file = new File("E:/" , "test.txt");
        if (file.exists()) {
            String name = file.getName();
            boolean isHidden = file.isHidden();
            long length = file.length();
            System.out.println("文件的名称：" + name);
            System.out.println("文件是否隐藏文件：" + isHidden);
            System.out.println("文件大小：" + length);

        } else {
            System.out.println("文件不存在");
        }
    }

    /*
    缓存流文件
    * */
    private static void bufferFile() {
        //BufferedInputStream
//        ZipInputStream

    }
}
