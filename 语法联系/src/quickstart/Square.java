package quickstart;

//正方形的面积
public class Square implements ICalculate {

    @Override
    public float getArea(float r) {
        return r * r;
    }

    @Override
    public float getCircumference(float r) {
        return r * 4;
    }

    @Override
    public float getArea(float a , float b) {
        return a * b;
    }
}
