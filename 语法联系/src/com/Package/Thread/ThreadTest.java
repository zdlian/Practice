package com.Package.Thread;

public class ThreadTest {

    /*线程的优点有如下几条：
    创建一个线程比创建一个进程的代价要小 线程的切换比进程间的切换代价小
    充分利用多处理器
    数据共享（数据共享使得线程之间的通信比进程间的通信更高效）
    快速响应特性（在系统繁忙的情况下，进程通过独立的线程及时响应用户的输入 ）
    在单线程中，程序代码按调用顺序依次往下执行，如果需要一个进程同时完成多段代码的操作，就需要产生多线程。*/
    public static void main(String[] args) {

        MyThread myThread = new MyThread();
        MyThread myThread1 = new MyThread();
        MyThread myThread2 = new MyThread();
        myThread.start();
        myThread1.start();
        myThread2.start();
        //　线程的生命周期包含出生状态、就绪状态、运行状态、等待状态、休眠状态、阻塞状态和死亡状态7种状态
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //线程安全
        ThreadSafe test = new ThreadSafe();
        Thread tA = new Thread(test);        //实例化4个线程
        Thread tB = new Thread(test);
        Thread tC = new Thread(test);
        Thread tD = new Thread(test);
        tA.start();        //启动线程
        tB.start();
        tC.start();
        tD.start();
    }
}
