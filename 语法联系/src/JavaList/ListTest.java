package JavaList;


import java.util.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/*
 * 测试集合类
 * */
public class ListTest {

    /* ArrayList和LinkedList的大致区别如下:
             1.ArrayList是实现了基于动态数组的数据结构，LinkedList基于链表的数据结构。
             2.对于随机访问get和set，ArrayList觉得优于LinkedList，因为LinkedList要移动指针。
             3.对于新增和删除操作add和remove，LinedList比较占优势，因为ArrayList要移动数据。*/
    public static void main(String[] args) {

        String aString = "A", bString = "B", cString = "C", dString = "D", eString = "E";
        List <String> list = new LinkedList <String>();  // 创建list集合对象
        list.add(aString);
        list.add(bString);
        list.add(cString);
        // 输出语句，用迭代器
        for (String arr : list) {
            System.out.println(arr);
        }

        list.set(1 , dString); //索引为1的对象修改
        Iterator <String> iter = list.iterator();        // 创建集合迭代器
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }

        List <String> list2 = new ArrayList <String>();
        list2.add("dog");
        list2.add("cat");
        list2.add("fish");
        list2.add("cat");    //重复值

        //List 有序集合 Set 无序的集合
        Set <String> set = new HashSet <String>();
        set.addAll(list2);
        if (!list2.isEmpty()) {
            Iterator <String> it = set.iterator();
            while (it.hasNext()) {
                System.out.println(it.next() + " ");
            }
        }

        //键值对
        Map map = new HashMap();
        map.put(1 , "zhang");
        map.put(2 , "dong");
        map.put(3 , "lian");
        for (int i = 1; i <= 3; i++) {
            System.out.println(String.format("Key=%s;Value=%s" , i , map.get(i)));
        }

        //异常
        int result = 1 / 0;
        try {
            System.out.println(result);
        } catch (Exception e) {
            System.out.println("抛出异常：" + e.getMessage());
        }

        int x = -1;
        /*if(x < 0) {
            throw new Exception("程序异常，x不能小于0。");
        }*/


    }

}
