package hello;

/*
 * 抽象类
 */
abstract class Animal {
    public String animal;

    public Animal() {
    }

    /* 抽象类中的方法都是抽象的，必须由子类实现
     * */
    public abstract void voice();
}
