package com.Package.DB;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MysqlTest {

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        /*//连接数据库
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        }
        String url= "jdbc:mysql://localhost:3306/himall2_4";
        String user="root";
        String password="111qqq";
        Connection conn=DriverManager.getConnection(url,user,password);*/

        //测试连接
        DBUtils db = new DBUtils();
        db.openConnection();
        String sql = "select * from himall_members";
        try {
            ResultSet rs = db.execQuery(sql);
            int id, sex;
            String username, address;
            System.out.println("id\t姓名\t性别\t地址\t");
            if (!rs.wasNull()) {
                while (rs.next()) {
                    String _sex;
                    id = rs.getInt("Id");
                    username = rs.getString("username");
                    sex = rs.getInt("sex");
                    address = rs.getString("address");
                    if (sex == 1) {
                        _sex = "男";
                    } else if (sex == 2) {
                        _sex = "女";
                    } else {
                        _sex = "未知";
                    }

                    System.out.println(id + "\t" + username + "\t" + _sex + "\t" + address);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



        /*String sql= "select * from himall_members";
        Statement statement=conn.createStatement(); //创建一个 statement 对象
        ResultSet rs= statement.executeQuery(sql); //执行查询

        int id, sex;
        String username, address;
        System.out.println("id\t姓名\t性别\t地址\t");
        if(!rs.wasNull())
        {
            while (rs.next())
            {
                String _sex;
                id = rs.getInt("Id");
                username = rs.getString("username");
                sex = rs.getInt("sex");
                address = rs.getString("address");
                if(sex == 1) {
                    _sex = "男";
                } else if(sex == 2) {
                    _sex = "女";
                } else {
                    _sex = "未知";
                }

                System.out.println(id + "\t" + username + "\t" + _sex + "\t" + address);
            }
        }
        //当前时间
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        String now = df.format(new Date());
        //插入数据
        String  insertSql="insert into Himall_Members(UserName,Password,PasswordSalt, Sex, Address,CreateDate,TopRegionId,RegionId,Disabled,LastLoginDate,OrderNumber,Expenditure,Points) values('张三','123131','euwiruew','1','陕西西安','2018-08-09',0,0,0,'2019-09-09',0,0,0)";
        Statement statement1 =conn.createStatement();
        statement1.executeUpdate(insertSql);
        statement1.close();*/

    }
}
