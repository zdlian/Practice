package com.Package.ClientServer;

import java.io.IOException;
import java.net.*;
import java.io.*;
import java.util.*;

public class ClientTest {

    public static void main(String[] args) throws IOException {
        String host = "127.0.0.1";
        int port = 9099;

        System.out.println("-----------服务器启动-----------");

        Socket client = new Socket(host , port);
        Writer writer = new OutputStreamWriter(client.getOutputStream());
        Scanner in = new Scanner(System.in);
        writer.write(in.nextLine());
        writer.flush();
        writer.close();
        client.close();
        in.close();
    }
}
