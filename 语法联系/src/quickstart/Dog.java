package quickstart;

///继承 ，子类对父类的扩展

public class Dog extends Animal {

    public Dog() {
    }

    @Override
    public void voice() {
        System.out.println("Dog  voice..");
    }

}
